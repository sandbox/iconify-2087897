Calendar_Plus Overview

The Calendar module (http://drupal.org/project/calendar) currently uses tables 
for display. Tables can be styled to resize based on the dimensions of the 
containing element, but simply resizing doesn't make an element responsive. 
While tables are scalable via inherited dimensions, table cells are displayed in 
discreet rows and columns. As the table resizes, the contents either scale 
accordingly or become crammed into the smaller space. This is not responsive nor 
is it particularly usable on a mobile device.

We wanted to create a truly responsive experience so that on different display 
sizes, the presentation can be reorganized for a more device-appropriate 
experience. In particular, we wanted the calendar to display as a table view on 
larger screens and as a list on smaller screen sizes.

We wanted to create a viable responsive version of the calendar without forking 
the code or relying on hackish CSS-only approaches that attempt to force table 
elements to behave like other elements. Our solution uses an unordered list that 
is styled to display like a table on devices that are wider than 768 pixels, but 
displays as a list on devices narrower than 768 pixels.

The way the Calendar module is written, it is possible to theme the output using 
Drupal's standard theming paradigm, which is to say, by creating custom tpl.php 
files and adding them to the active theme. Unfortunately, however, the tables 
output is tightly coupled to the logic of the module code so there was no way to 
use any element other than a table by simply creating new template files.

Even while I knew I'd have to recode part of the module, I didn't want to 
reinvent the wheel unnecessarily. At this point I created a few restrictions for 
myself. Which were:

1.  Keep most as much of the heavy lifting in terms of 
administration/configuration and business logic in Calendar.

2.  Calendar_Plus will run invisibly as an extension of the functionality of 
Calendar but not as a replacement.

3.  Calender_Plus must be completely self-contained so using it is simply a 
matter of installing and enabling.

4.  Calendar_Plus must change the Calendar output only. It must not in any way 
modify the stored configuration or data.

5.  Calendar_Plus must implement existing Drupal hooks and API’s without relying 
on hacks or invented conventions.

The Calendar_Plus module changes where Calendar looks for the templates, and 
over-rides those functions in Calendar that have the concept of table-based 
output hard-coded, using standard Drupal hooks.

When the Calendar_Plus module is enabled, it changes the output of the Calendar 
module but doesn't affect any other functionality. Calendar events are created 
and managed exactly as they always are and the module configuration is handled 
exactly the same. There is no admin interface for the Calendar_Plus module.

The Code

I very much like the idea of not re-inventing the wheel. Calendar is 
well-established and very popular so I'm not trying to compete or replace the 
module. The changes to the code in Calendar_Plus are fairly minimal. In fact, 
there  are only two functions that were significantly changed from their 
Calendar counterparts:

calendar_build_month
calendar_build_week_day

I'm going to go through the most important changes. I think the specifics of how 
I've accomplished the changes is less important than the principles since you 
can use these same principles in similar use cases you might encounter in other 
modules.

First, In the .INFO file, I'm adding Calendar, Views, Date_api, and date_views 
as dependencies. Calendar_Plus is useless as a stand-alone module, so I want to 
make sure to notify potential users that they need other modules installed in 
order to use this.

calendar_plus.info

  name = Calendar Plus
  description = Converts the Calendar module output to responsive HTML/CSS
  package = Date/Time
  core = 7.x

  dependencies[] = views
  dependencies[] = date_api
  dependencies[] = date_views
  dependencies[] = calendar

A module registers a Views plugin via hook_views_plugins. With Calendar_Plus, 
we're not registering a new plugin but rather changing the details of the 
existing Calendar plugin. In this hook, we tell Drupal where to find the assets 
for the plugin, the name of the class to use to render the View output, etc.
 
/**
 * Implementation of hook_views_plugins
 */
function calendar_plus_views_plugins() {

  $views_path  = drupal_get_path('module', 'views');
  
  // Point Drupal to the calendar_plus module path
  
  $module_path = drupal_get_path('module', 'calendar_plus');
  $theme_path  = $module_path;
  module_load_include('inc', 'calendar_plus', 'theme/theme');

  $base = array_keys(date_views_base_tables());

  $data = array(
  
    // Notice we register this plugin for the Calendar module
    
    'module' => 'calendar',
    
    'style' => array(
      'calendar_style' => array(
        'title' => t('Calendar'),
        'help' => t('Present view results as a Calendar.'),
        
        // Use the new calendar_plus_plugin_style class as the handler
        
        'handler' => 'calendar_plus_plugin_style', 
        
        'path' => "$module_path/includes",
        'theme' => 'calendar_style',
        'theme file' => 'theme.inc',
        'theme path' => "$theme_path/theme",
      ),
    ),
    'row' => array(
      'calendar_entity' => array(
        'title' => t('Calendar Entities'),
        'help' => t('Displays each selected entity as a Calendar item.'),
        
        // Use the calendar_plus_plugin_row to process view data rows
        
        'handler' => 'calendar_plus_plugin_row',
        
        'theme' => 'views_view_fields',
        'path' => "$module_path/includes",
      ),
    ),
  );
  return $data;
}

Also in the calendar_plus.module file, we need to implement hook_theme to tell 
Drupal where to find the new templates:

/**
 * Implements hook_theme().
 */
function calendar_plus_theme() {

  // Point Drupal to the calendar_plus module path to find templates for 
  // Calendar

  $module_path = drupal_get_path('module', 'calendar_plus');
  $base = array(
    'file' => 'theme.inc',
    'path' => "$module_path/theme",
  );
}

I don't want to redefine all of the logic from Calendar's theme.inc, so the 
Calendar_Plus version is just an empty file. I would just omit it but Drupal 
barks at me when I do. So we just include an empty version to make Drupal happy.

I'm not going to go into a lot of detail on the changes to the logic but I'll 
call out where most of the changes to logic were made. As I mentioned earlier, 
the logic in the Views plugin for Calendar is very tightly coupled to the notion 
of using tables, so the following functions needed to be re-written:

  calendar_build_month
  calendar_build_week_day
  calendar_build_week
  calendar_build_item

Very quickly I want to show you the structure of the module files and 
directories. Specifically I'd like to call out that the CSS and JavaScript for 
the module are included here rather than as part of the theme. The CSS and JS 
are for structure only. Aesthetic styling of the calendars should still be 
handled in the theme.

  calendar_plus/
  calendar_plus.info
    calendar_plus.install
    calendar_plus.module
    css/
      calendar_plus.css
    includes/
      calendar_plus_plugin_row.inc
      calendar_plus_plugin_style.inc
      calendar_plus.views_template.inc
      calendar_plus.views.inc
    js/
      calendar_plus.js
      jquery.equalheights.js
    theme/
      calendar-datebox.tpl.php
      calendar-day-overlap.tpl.php
      calendar-day.tpl.php
      calendar-item.tpl.php
      calendar-mini.tpl.php
      calendar-month-col.tpl.php
      calendar-month-multiple-entity.tpl.php
      calendar-month-row.tpl.php
      calendar-month.tpl.php
      calendar-style.tpl.php
      calendar-week-overlap.tpl.php
      calendar-week.tpl.php
      calendar-year.tpl.php
      theme.inc
        
Next, I want to point out which files Calendar_Plus is over-riding. It isn't 
necessary to over-ride them all because most of the logic in Calendar is fine 
as-is. In particular, since Calendar processes the rows and columns as a table, 
it is necessary to redefine the data structure that gets returned to the theming 
layer.

  calendar_plus/
  includes/
        calendar_plus_plugin_style.inc
  
When the Calendar_Plus module is loaded, the calendar_plus_views_plugins hook is 
called to register the module as Views plugin. This is the class that is 
referenced in the handler callback in calendar_plus.module function that 
registers the plugin.
      
  calendar_plus_plugin_row.inc
  
This is where we reorganize the data structure for the calendar as a list 
instead of a table of rows X columns.
  
  calendar_plus.views_template.inc
  calendar_plus.views.inc
  
Most of the code in these two files is identical to the originals, but there 
were a couple of places where function calls needed to be changed to point to 
Calendar_Plus's versions instead of Calendar's
      
The last thing I'd like to call attention to is the theme template files for 
Calendar_Plus. Most of these have been left unchanged from the Calendar 
versions. The reason I'm including them here is that, if you will recall, in the 
calendar_plus.module file, we changed the location where Drupal looks for the 
theme files. So even though most of the files remain unchanged, Drupal needs to 
be able to find them.

  theme/
    calendar-datebox.tpl.php
    calendar-day-overlap.tpl.php
    calendar-day.tpl.php
    calendar-item.tpl.php
    calendar-mini.tpl.php
    calendar-month-col.tpl.php
    calendar-month-multiple-entity.tpl.php
    calendar-month-row.tpl.php
    calendar-month.tpl.php
    calendar-style.tpl.php
    calendar-week-overlap.tpl.php
  calendar-week.tpl.php
    calendar-year.tpl.php
    theme.inc
        
Now, that said, there were significant changes to a couple of template files:

  calendar-datebox.tpl.php
  calendar-mini.tpl.php
  calendar-month.tpl.php
        
These files are where most of the structure of the calendars is defined. The 
modifications mostly involved changing the enclosing HTML from TABLE, TR, and TD 
elements to UL and LI elements.

I welcome feedback and suggestions.
